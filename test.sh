#!/bin/sh -x
vagrant destroy -f && vagrant up
vagrant ssh alice -- "echo hello world | mutt -s greeting vagrant@bob.lan"
vagrant ssh alice -- sudo service opendkim status
vagrant ssh alice -- dig txt mail._domainkey.alice.lan
vagrant ssh bob -- cat /var/mail/vagrant
vagrant ssh bob -- cat /var/mail/vagrant | grep Authentication-Results
vagrant ssh nameserver -- tail -3 /etc/nsd/alice.lan
vagrant ssh bob -- sudo cat /var/log/mail.log | grep opendkim
