#!/bin/sh

apt-get -y install dnsmasq
cat << EOF > /etc/dnsmasq.d/local.conf
server=/alice.lan/192.168.41.15
server=/bob.lan/192.168.41.15
EOF
service dnsmasq restart
