DKIM Fail
=========

Im trying to get DKIM to work in this little sandbox.

The vagrantfile starts two mail servers, an "isp" (dns resolver),
and an authoritative nameserver.


Process
-------

see test.sh

0. destroy previous run
1. the isp is made. all it does is run dnsmasq and set nameserver as the nameserver for alice.lan and bob.lan.
2. each mailserver is made. the last step is to take the private dkim key and add it to the base zone file in the nsd folder
3. the nameserver is made
4. some tests are run
  1. clean vagrant run
  2. alice sends a message to bob
  3. make sure opendkim is running
  4. show the dkim txt record in the zone file
  5. make sure the txt record accessable to the mail servers
  6. show the sent, signed message
  7. show Authentication-Results in sent, signed message

Results
-------

full output from a run is in sample-output.txt, so you dont have to download and run this yourself unless you want to poke tat the result. you can skip the setup by scrolling past the text cow drawings. 

the sample output also cats alice.lans zone file at the end, because the full zone files are not tracked by git. they are created by adding dkims text output to the base zone file that you see in the nsd folder.

here are what i hope are the relevant parts.

The mail is sent, but opendkim seems to not be able to find the key,

    Mar  3 02:22:56 opendkim[6095]: CF8C34019A: alice.lan [192.168.41.20] not internal
    Mar  3 02:22:56 opendkim[6095]: CF8C34019A: not authenticated
    Mar  3 02:22:56 opendkim[6095]: CF8C34019A: no signing domain match for 'alice.lan'
    Mar  3 02:22:56 opendkim[6095]: CF8C34019A: no signing subdomain match for 'alice.lan'
    Mar  3 02:22:57 opendkim[6095]: CF8C34019A: key retrieval failed (s=mail, d=alice.lan): 'mail._domainkey.alice.lan' record not found

heres the txt record in the zone file.

    mail._domainkey	IN	TXT	( "v=DKIM1; k=rsa; "
    	  "p=iaiaiacthulhufghnaten" )  ; ----- DKIM key mail for alice.lan

Heres the answer section of dig txt mail._domainkey.alice.lan

    ;; ANSWER SECTION:
    mail._domainkey.alice.lan. 86400 IN	TXT	"v=DKIM1\; k=rsa\; " "p=iaiaiacthulhufghnaten"

Vagrant and ansible
-------------------

Vagrant is a tool to create and manage disposable virtual environments. Normally, one 
would run "vagrant up". but, because im lazy, i put that in ./test.sh. so, if you have
vagrant installed, you can run the script and, in a few minutes, you'll have this test
network running on your host, and you can ssh into the virtual machines and see what i
did. or, you can just look at the vagrant files, scripts and playbooks (the .yml files) 
to see what i did.

https://www.vagrantup.com/docs/

Ansible is a configuration mangement tool. The .yml files are ansible playbooks. They should be easy to read even if your not familiar with ansible.

https://docs.ansible.com/

